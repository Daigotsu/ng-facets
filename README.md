### Angular 2 implementation of telescope Facet control ###

You can clone and run /dist/index.html to check results

### How do I get set up for development? ###

* `npm install` to install all dependencies
* `npm run server:dev` to run webpack-dev-server with watch compilation of resources
* `npm run build:prod` to run _production_ build. Outputs all files in `dist` directory


## Results

#### WF
Тестовый контрол WF'a - посмотреть работу можно на ветке feature/facets-ui по адресу /ui/wf/dev/test/control/~/epam/search/controls/facets/epsrchFacetsTest.wfc
мои результаты - http://i.imgur.com/4jpyBvp.png.
если смотреть без профайлера, то честный ренедринг всей даты(без отложенного добавления рендеринга) занимает от ~750-850ms + Layout (~230ms). Реализация с использованием Wf Data даёт оверхед по времени  ~30%. 
Текущая имплементация update выглядит хорошо - 80ms js + 80 layout

#### Angular 2
Реализацию можно посмотреть тут https://bitbucket.org/Daigotsu/ng-facets временно открыл доступ, т.к. есть там sensitive информация.
Не совсем правильно что взял Angular 2 т.к. он ещё beta  и скорее всего многих вещей ещё нет в плане оптимизаций, но с другой стороны заявляют что он и так быстр. Я старался делать максимально близко к реализации от wf. Однако, если начальный ренедринг выглядит идентично в плане реализации, то вот update нет. Обновляется весь сет данных, и видимо Angular почти полностью всё перерендеривает. В wf обновляется по отдельности каждый фасет, плюс добавляются новые если есть. Подход текущий angular с точки зрения имплементации - это никаких затрат, но с точки зрения перфоманса - проседает. Покопаю ещё на эту тему, но вроде в angular 1 эти проблемы был призван решить `track by`. 
Вот что получил(учитывайте оверхед dev tool) 
на ренденринг всех фсетов ~730ms + 110ms Layout. Update данных сопровождается теми же затратами, хотя вот апдейт new на old занял ~590ms на js. И последующие частые переключения данных  занимали стабильно ~600ms
http://i.imgur.com/zd5NR9A.png

React
coming soon...