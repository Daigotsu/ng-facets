"use strict";

var CTRL_CLASS = $module.id.sid;
var CTRL_CLASS_HIDDEN = CTRL_CLASS + "-hidden";

var CTRL_FACET_ITEM_CLASS = CTRL_CLASS + "__facet";

var CSSCLS_FACETS_COLLAPSED = "glyphicon-eye-close epsrchFacets__btnCollapseAll-collapsed";
var CSSCLS_FACETS_EXPANDED = "glyphicon-eye-open";

var FACET_CTRL_TID = $module.expandId("~!/controls/epsrchFacet.wfc");

var quickSearchApi = $module.require("~/epam/e3sui/controls/e3suiQuickSearch/e3suiQuickSearch.jsm").jsm.api,
    e3suiApi = $module.require("~/epam/e3sui/js/e3suiApi.jsm").jsm.api,
    epsrchFacetSortersApi = $app.require("~!/js/epsrchFacetSorters.jsm").jsm.api,
    /** @type {epsrchFacetConfigurator} */
    facetConfiguratorApi = $app.require("~!/controls/epsrchFacetConfigurator/epsrchFacetConfigurator.jsm").jsm.api;

var epsrchApiWfEvents;
$module.require("~/epam/search/js/epsrchSearchApiWfEvents.jsm").jsm.onload(function _epsrchSearchApiWfEvents_onload(evtModule) {
    epsrchApiWfEvents = evtModule.wfEvents;
});

// If some facet controls `epsrchFacet.wfc` rendered in lazy mode,
// this collection will contain objects for deferred render
var deferredFacetItems = [];

$module.extend(
    /** @namespace epsrchFacets */
    {
        epsrchFacets:            new epsrchFacets(),
        renderFacet:             renderFacet,
        gatherQueryFacets:       _epsrchFacets_gatherQueryFacets,
        mergeFacetData:          _epsrchFacets_mergeFacetData,
        doConfigureFacets:       doConfigureFacets,
        onApplyFilterAll:        onApplyFilterAll,
        onCancelFilterAll:       onCancelFilterAll,
        _btnCollapseAll_onClick: _btnCollapseAll_onClick,
        deferFacetRender:        deferFacetRender
    });

/**
 * Public API of the Facets control.
 * @constructor
 * @extends {HTMLElement}
 */
function epsrchFacets() {

    /**
     * Initial data object for this facet.
     */
    this.facets = new Wf.AutoRwPropDef();

    /**
     * @type {e3suiQuickSearch}
     */
    this.filterCtrl = new Wf.AutoChildPropDef();

    /**
     * @type {HTMLElement}
     */
    this.body = new Wf.AutoChildPropDef();

    /**
     * @type {HTMLElement[]}
     */
    this.facetNodes = new Wf.AutoChildPropDef();

    /**
     * Pass-through redeclaration of the bubbleable inner epsrchFacet.facetclick event
     */
    this.facetclickEvent = new Wf.DomEventDef("b");

    /**
     * Specifies whether the control should be hidden after rendering.
     * A template parameter.
     */
    this.hidden = new Wf.AutoRwPropDef();

    /**
     * Show the control.
     * @public
     */
    this.show = function epsrchFacets__show() {
        this.classList.remove(CTRL_CLASS_HIDDEN);
    };

    /**
     * Hide the control.
     * @public
     */
    this.hide = function epsrchFacets__hide() {
        this.classList.add(CTRL_CLASS_HIDDEN);
    };

    /**
     * Toggle the control visibility.
     * @param {Boolean} [value] If passed and truthy, the control will be made visible. If passed and falsy, the control will be made hidden.
     *  If not passed, the control’s visibility will be changed to the opposite value.
     * @public
     */
    this.toggle = function (value) {
        if (value == null) {
            value = !this.isVisible();
        }

        if (value) {
            this.show();
        } else {
            this.hide();
        }
    };

    /**
     * Return true if the control is visible, or false otherwise.
     * @returns {Boolean}
     * @public
     */
    this.isVisible = function () {
        return !this.classList.contains(CTRL_CLASS_HIDDEN);
    };

    /**
     * Cancels the 'filter all'
     *
     * @returns {Number} Number of the unfiltered facets.
     */
    this.cancelFilterAll = function epsrchFacets_cancelFilterAll(fromUser) {
        var facetsBody = this.body,
            $facets = $("> .epsrchFacet", facetsBody),
            cUnfiltered = 0;

        if (!fromUser)
            this.filterCtrl.cancelFilter();

        for (var i = 0; i < $facets.length; i++) {
            var facet = $facets[i];
            var $captionNameNode = $(facet.captionNameNode);
            var captionText = $captionNameNode.text();

            $captionNameNode.html(captionText);
            if (facet.unfilter())
                cUnfiltered++;
            facet.unhide();
        }

        return cUnfiltered;
    };

    /**
     * Scrolls body with facets to given position.
     *
     * @param {Number} offset   New vertical scroll offset
     */
    this.scrollTo = function epsrchFacets_scrollTo(offset) {
        this.facetScroller.scrollTo(undefined, offset);
    };

    /**
     * See epsrchFacets.gatherQueryFacets().
     *
     * @param {SearchQuery} query
     */
    this.gatherQueryFacets = function epsrchFacets_gatherQueryFacets(query) {
        _epsrchFacets_gatherQueryFacets(query, this);
    };

    /**
     * Merges old facet data with one that came with request
     *
     * @param {object} newData
     *      New data with facets, in the same format consumed originally by "epsrchFacets.wfc".
     * @param {object} oldData
     *      Old data with facets, in the same format consumed originally by "epsrchFacets.wfc".
     * @param {String} keepDataFor
     *      If provided, specifies id of facet, for which counters should not be set to '0' if absent in new data
     */
    this.getMergedFacetData = function epsrchFacets_getMergedFacetData(newData, oldData, keepDataFor) {
        return _epsrchFacets_mergeFacetData(newData, oldData, keepDataFor);
    };

    /**
     * Scrolls facet with given ID into view.
     *
     * @param {string} facetId
     * @param {boolean} highlight
     */
    this.gotoFacet = function epsrchFacets_gotoFacet(facetId, highlight) {
        var facets = this.facetNodes;

        for (var i = 0; i < facets.length; i++) {
            var facet = facets[i];

            if (facet.$id === facetId) {
                this.scrollTo(facet.offsetTop);
                facet.expand();
                if (highlight) {
                    var currentBgColor = $(facet).css("backgroundColor") || "#f5f5f5";

                    $(facet).css({backgroundColor: "#E1D56C"});
                    $(facet).animate(
                        {backgroundColor: currentBgColor}, 500, null,
                        function () {
                            $(facet).css("backgroundColor", "");
                        });
                }
                break;
            }
        }
    };

    /**
     * Recursively sets the checked state of all facets in provided 'facets' array.
     *
     * @param {array} facets
     * @param {boolean} [checked]
     */
    this.checkFacets = function epsrchFacets_checkFacets(facets, checked) {
        if (!facets)
            return;

        for (var n in facets) {
            var facet = facets[n];

            facet.checked = checked;
            if (facet.checkbox)
                facet.checkbox().checked = checked;
            this.checkFacets(facet.facets, checked);
        }
    };

    /**
     * Collapses/expands all facets
     *
     * @param {boolean} collapse
     * @param {HTMLElement} srcBtn
     */
    this.collapseAll = function epsrchFacets_collapseAll(collapse, srcBtn) {
        if (collapse === undefined)
            collapse = true;

        var $facetsCtrl = $(this);
        var $facets = $("div.epsrchFacet__tn-collapsible", $facetsCtrl);
        var $btn = $(srcBtn);

        if (collapse) {
            $btn.removeClass(CSSCLS_FACETS_EXPANDED)
                .addClass(CSSCLS_FACETS_COLLAPSED);
        }
        else {
            $btn.removeClass(CSSCLS_FACETS_COLLAPSED)
                .addClass(CSSCLS_FACETS_EXPANDED);
        }

        for (var i = 0; i < $facets.length; i++) {
            var facet = $facets[i];

            if (collapse)
                facet.collapse();
            else
                facet.expand();
        }

    };

    this.__onstart = function () {
        //debugger;
        this.timer = Wf.profiler.pusht("FACETS_START");
    };

    this.__onend = function () {
        var ctrl = this;
        ctrl.toggle(!ctrl.hidden);

        // Control can be rendered without facets if no search results,
        if (ctrl.body) {
            $(ctrl.body).keydown(onKeyDownAllFacets);
            _initFacetsSorting(ctrl);
        }

        if (deferredFacetItems.length > 0) {
            _renderDeferredFacetItems(ctrl);
        }

        Wf.profiler.popt(this.timer);
    }
}

/**
 * Gathers checked facet values into target query object.
 *
 * @param {SearchQuery} query
 *      Query object to be filled
 *
 * @param {HTMLElement} [container]
 *      When invoked not on epsrchFacets control, but as moduleApi - defines the DOM
 *      node containing facets. To be used when facets are generated individually by
 *      invoking template.
 */
function _epsrchFacets_gatherQueryFacets(query, container) {
    //var timer = Wf.profiler.pusht("_epsrchFacets_gatherQueryFacets");

    if (!container) {
        container = this;
    }

    var facets = $(".epsrchFacet", container),
        i, facet, qfacets = {}, qfacet;

    for (i = 0; i < facets.length; i++) {
        facet = facets[i];
        qfacet = facet.checkedItems;
        if (Object.keys(qfacet.facets).length > 0) {
            qfacets[qfacet.facetId] = qfacet;
        }
    }

    query.facets = qfacets;

    //Wf.profiler.popt(timer);
}

/**
 * Merges old facet data with one that came with request
 *
 * @param {object} newViewData
 *      New data with facets, in the same format consumed originally by "epsrchFacets.wfc".
 * @param {object} oldViewData
 *      old data based on which facets were built
 * @param {String} keepDataFor
 *      If provided, specifies id of facet, for which counters should not be
 *      set to '0' if absent in new data
 */
function _epsrchFacets_mergeFacetData(newViewData, oldViewData, keepDataFor) {
    var oldFacets = oldViewData.facets;
    var newFacets = newViewData.facets;
    var facetSettings = newViewData.facetSettings;
    var facetShortName;

    if (!newFacets || _.isEmpty(newFacets)) {
        newFacets = {};
        //no data from server. no results found. set everything in old data to 0
        for (var facetId in oldFacets) {

            facetShortName = facetId.replace(".untouchable", "");
            if (facetSettings && facetSettings[facetShortName].visibility === "hidden") {
                continue; //skip hidden facets
            }
            if (!_epsrchFacets_facetHasCheckedItems(oldFacets[facetId])) {
                continue; //skip missing unchecked facets
            }

            newFacets[facetId] = oldFacets[facetId];
            var newFacet = newFacets[facetId];

            if (newFacet.ctrl) { // remember 'show more/less' and collapsed state
                newFacet.isMore = newFacet.ctrl().isMore;
                newFacet.collapsed = newFacet.ctrl().collapsed;
            }

            for (var facetItemName in newFacet.facets) {
                if (facetId !== keepDataFor) {
                    newFacet.facets[facetItemName].count = 0;
                }
                else { //we keep counters for specified facet
                    newFacet.facets[facetItemName].obsolete = true;
                }
            }
        }
    }
    else {
        for (var oldFacetName in oldFacets) {
            var oldFacet = oldFacets[oldFacetName];
            var newFacet = newFacets[oldFacetName];

            if (newFacet) {
                if (oldFacet) {

                    // remember 'show more/less' and collapsed state
                    newFacet.isMore = !!oldFacet.isMore;
                    newFacet.collapsed = !!oldFacet.collapsed;

                    //update facet items in new data with zeroed items from old data
                    for (var facetItemName in oldFacet.facets) {
                        if (!newFacet.facets[facetItemName]) {
                            newFacet.facets[facetItemName] = oldFacet.facets[facetItemName];

                            if (oldFacetName !== keepDataFor) {
                                newFacet.facets[facetItemName].count = 0;
                            }
                            else {
                                newFacet.facets[facetItemName].obsolete = true;
                            }
                        }
                        else {
                            if (oldFacetName === keepDataFor) {
                                //save previous order to prevent jumping of item of active facet
                                newFacet.facets[facetItemName].order = oldFacet.facets[facetItemName].order;
                            }
                        }
                    }
                }
            } else {
                //add whole facet from old data with its items set to '0'
                facetShortName = oldFacetName.replace(".untouchable", "");

                if (facetSettings && facetSettings[facetShortName].visibility === "hidden") {
                    continue; //skip hidden facets
                }
                if (!_epsrchFacets_facetHasCheckedItems(oldFacet)) {
                    continue; //skip missing unchecked facets
                }
                // remember 'show more/less' and collapsed state
                oldFacet.isMore = !!oldFacet.isMore;
                oldFacet.collapsed = !!oldFacet.collapsed;

                newFacets[oldFacetName] = oldFacet;
                for (var facetItemName in oldFacet.facets) {
                    if (oldFacetName !== keepDataFor) {
                        newFacets[oldFacetName].facets[facetItemName].count = 0;
                    }
                    else {
                        newFacets[oldFacetName].facets[facetItemName].obsolete = true;
                    }
                }
            }
        }
    }

    if (newFacets[keepDataFor]) { //if at least one facetItem is checked we wont sort it
        var hasChecked = _epsrchFacets_facetHasCheckedItems(newFacets[keepDataFor]);
        _epsrchFacets_sortFacetItems(newFacets, facetSettings, hasChecked ? keepDataFor : "");
    } else {
        _epsrchFacets_sortFacetItems(newFacets, facetSettings);
    }

    return newFacets;
}

function _epsrchFacets_facetHasCheckedItems(facet) {
    for (var facetItemId in facet.facets) {
        var facetItem = facet.facets[facetItemId];
        if (facetItem.checked) {
            return true;
        }
    }
    return false;
}

/**
 * Append context for deferred facet items render
 * @param {epsrchFacet} ctx
 */
function deferFacetRender(ctx) {
    deferredFacetItems.push(ctx);
}

/**
 * Apply deferred render for all lazy facets
 * @param {epsrchFacets} ctrl
 * @private
 */
function _renderDeferredFacetItems(ctrl) {
    var i, len = deferredFacetItems.length;

    var renderFnList = [];
    setTimeout(function () {
        for (i = 0; i < len; i++) {
            var defFacet = deferredFacetItems[i];
            renderFnList.push(renderFacetItems.bind(null, defFacet));
        }
        deferredFacetItems.length = 0;
        applyRenderFn(renderFnList.shift());
    }, 500);

    function applyRenderFn(renderFn) {
        if (renderFn) {
            setTimeout(function() {
                renderFn();
                applyRenderFn(renderFnList.shift());
            }, 30)
        } else return false;
    }
}

/**
 * Render epsrchFacet control to facets body
 * @param {FacetContainer} facetData
 * @param {HTMLElement} [nextSibling] Facet control Can be append before existed rendered facet
 */
function renderFacet(facetData, nextSibling) {
    var renderOptions = {
        dst:    Wf.qs(".epsrchFacets__body"),
        append: true,
        data:   {facetData: facetData, "facetClass": CTRL_FACET_ITEM_CLASS}
    };

    if (nextSibling) {
        renderOptions["before"] = nextSibling;
    }

    Wf.renderRgn(null, FACET_CTRL_TID, "", renderOptions);
}

/**
 * Render epsrchFacet~facetItems region to facet `ctx` body
 * @param {epsrchFacet} ctx
 */
function renderFacetItems(ctx) {
    var renderOptions = {
        dst:    ctx.itemsContainer,
        append: true,
        data:   {items: ctx.viewData.items, start: 6, limit: ctx.maxFacetItemsLimit}
    };

    Wf.renderRgn(null, FACET_CTRL_TID, "facetItems", renderOptions);
}

function _btnCollapseAll_onClick(event) {
    /** @type epsrchFacets */
    var facetsCtrl = this.$wfc;
    var $btn = $(this);
    var expanded = $btn.hasClass(CSSCLS_FACETS_EXPANDED);

    facetsCtrl.collapseAll(expanded, this);
}

function doConfigureFacets(event) {
    // When doConfigureFacets() is called on a link, prevent its default action
    event.preventDefault();

    $app.require("~./controls/epsrchFacetConfigurator/epsrchFacetConfigurator.jsm").jsm.onload(function () {
        var entityId = $page.views["~/epam/search/v-epsrchResponse"].entityType;
        $app.api.showFacetsConfigurator(entityId, "facetConfiguratorDialog");
    });
}

function getParentSection(ctrl) {
    return $(ctrl).closest(".epsrchFacets");
}

function getParentBody(ctrl) {
    return $(".epsrchFacets__body", getParentSection(ctrl));
}

function onApplyFilterAll(event) {
    var facetsBody = getParentBody(event.target),
        $facets = $("> .epsrchFacet", facetsBody),
        text = event.args.text,
        regexp = quickSearchApi.getSearchRegexp(text);

    for (var i = 0; i < $facets.length; i++) {
        var facet = $facets[i];
        var cVisible = facet.applyFilter(text);
        var captionMatches = false;
        var $captionNameNode = $(facet.captionNameNode);
        var captionText = $captionNameNode.text();
        if (captionText.match(regexp)) {
            $captionNameNode.html(quickSearchApi.highlight(captionText, regexp, "epsrchFacet__item-filterHL"));
            captionMatches = true;
        }
        else {
            $captionNameNode.html(captionText);
        }

        if (cVisible > 0 || captionMatches) {
            facet.unhide();
            facet.expand();
            if (!cVisible)
                facet.unfilter();
        }
        else {
            facet.hide();
        }
    }
}

function onCancelFilterAll(event) {
    event.target.$parentWfc.cancelFilterAll(true);
}

function _epsrchFacets_sortFacetItems(facets, facetSettings, keepDataFor) {

    function hasPriority(facet) {
        return facet.checked || (!facet.obsolete && facet.count != 0);
    }

    function toArray(facets) {
        var items = [];
        for (var facetItemId in facets) {
            facets[facetItemId].id = facetItemId;
            items.push(facets[facetItemId]);
        }
        return items;
    }

    function fromArray(array) {
        var facets = {};
        for (var i = 0; i < array.length; i++) {
            array[i].order = i;
            facets[array[i].id] = array[i];
        }
        return facets;
    }

    function facetComparatorDecorator(facetComparator) {
        return function (a, b) {
            if (hasPriority(a) && !hasPriority(b)) {
                return -1;
            } else if (!hasPriority(a) && hasPriority(b)) {
                return 1;
            } else {
                return facetComparator(a, b);
            }
        }
    }

    function getSorter(name) {
        return epsrchFacetSortersApi.getSorter(name) || epsrchFacetSortersApi.getDefaultSorter();
    }

    for (var facetId in facets) {
        if (facetId == keepDataFor) {
            continue;
        }
        var facet = facets[facetId];
        if (facet.facets) {
            var facetItems = toArray(facet.facets);
            var itemsComparator = getSorter(facetSettings[facetId.replace(".untouchable", "")].sorting).comparator;
            facetItems = facetItems.sort(facetComparatorDecorator(itemsComparator));
            facet.facets = fromArray(facetItems);
        }
    }
}

function onKeyDownAllFacets(event) {
    if ((event.key || event.keyCode) === 27 /* ESC*/) {
        // unfiltering via ESC
        var $section = $(".epsrchFacets"),
            facetsFilter = $(".epsrchFacets_filter", $section)[0];

        if (facetsFilter && facetsFilter.cancelFilter()) {
            event.preventDefault();
            event.stopPropagation();
            $(facetsFilter).focus();
        }
    }
}
/**
 * Initialize drag-n-drop sorting on facets
 * @param {epsrchFacets} ctrl
 * @private
 */
function _initFacetsSorting(ctrl) {
    Sortable.create(ctrl.body, {
        handle:     ".epsrchFacet__dragHandler",
        draggable:  "." + CTRL_FACET_ITEM_CLASS,
        ghostClass: "epsrchFacet__facetDragged",
        animation:  150,
        onUpdate:   _saveFacetsConfigOnSort
    });

    // TODO: Update with another image
    var dragIcon = document.createElement('img');
    dragIcon.src = "/ui/~/epam/search/controls/facet-drag-placeholder.png";
    var facets = Wf.asArray(Wf.qsAll("." + CTRL_FACET_ITEM_CLASS));
    facets.forEach(function (facet) {
        facet.addEventListener('dragstart', function (event) {
            event.dataTransfer.setDragImage(dragIcon, 0, 0);
            event.dataTransfer.effectAllowed = "move";
        });
    });
}

function _saveFacetsConfigOnSort(evt) {
    facetConfiguratorApi.getFacetConfig().then(function (config) {
        var newConfig = _updateSortOrder(config.options);
        facetConfiguratorApi.saveFacetConfig(newConfig, config.suiteId)
    });
}

function _updateSortOrder(oldOptions) {
    var facetElements = Wf.asArray(Wf.qsAll("." + CTRL_FACET_ITEM_CLASS)),
    // Get ordered list of visible facets names
        facetNamesOrderedList = facetElements.map(function (facetEl) {
            return facetEl.configId;
        }),
    // Get configs of visible facets
        onlyVisibleFacetsConfig = _.filter(oldOptions, function (facetConfig) {
            return facetNamesOrderedList.indexOf(facetConfig.facetId) !== -1;
        }),
    // Get priorities only in visible facets configs
        priorities = _getCurrentPrioritiesSortedList(onlyVisibleFacetsConfig);
    var newConfig = _.clone(oldOptions);

    // Update priority
    facetNamesOrderedList.forEach(function (facetConfigId, i) {
        newConfig[facetConfigId].priority = priorities[i];
    });

    return newConfig;
}

function _getCurrentPrioritiesSortedList(options) {
    return _.map(options, function (el) {
        return el.priority
    }).sort(function (a, b) {
        return a - b
    });
}
