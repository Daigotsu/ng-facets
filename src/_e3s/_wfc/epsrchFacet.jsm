"use strict";

/** @type {epsrchFacets} */
var e3suiQuickSearch = $module.require("~/epam/e3sui/controls/e3suiQuickSearch/e3suiQuickSearch.jsm").jsm.api;

/** @type {epsrchFacets} */
var facetsApi = $app.require("~./controls/epsrchFacets.jsm").jsm.api;

$module.require("~/wf/api/js/wftEvents.jsm").jsm.onload(function (wftEventsModule) {
    $module.handle(wftEventsModule.wfEvents.onAttachTemplate, function (wfEvent) {
        var template = wfEvent.args.template;
        _registerTemplateFormatter(template);
    });
});

// By default only this count of facet items visible on UI
var MAX_FACETITEMS_VISIBLE = 6;
// Max possible count of visible facet items on UI
var MAX_FACETITEMS = 300;

var FACET_ID_POSTFIX = ".untouchable";

$module.extend({
    epsrchFacet:    new epsrchFacet(),
    MAX_VISIBLE:    MAX_FACETITEMS_VISIBLE,
    MAX_FACETITEMS: MAX_FACETITEMS
});

var CTRL_CLASS = $module.id.sid,
    FACET_ITEM = CTRL_CLASS + "__facetItem";

var CLASSES = {
    FACET:       {
        HIDDEN:          CTRL_CLASS + "-hidden",
        COLLAPSED:       CTRL_CLASS + "-collapsed",
        FILTERED:        CTRL_CLASS + "-filtered",
        ITEMS_COLLAPSED: CTRL_CLASS + "-itemsCollapsed"
    },
    FACET_ITEM:  {
        DEFAULT:                "epsrchFacet__items epsrchFacet__facetItem ", // Used for class binding only
        HIDDEN:                 FACET_ITEM + "-hidden",
        OBSOLETE:               FACET_ITEM + "-obsolete",
        FORCE_HIDE:             FACET_ITEM + "-forceHide",
        FORCE_SHOW:             FACET_ITEM + "-forceShow",
        FORCE_FILTER_HIGHLIGHT: FACET_ITEM + "-filterHighlight",
        FORCE_FILTER_OBSOLETE:  FACET_ITEM + "-obsolete"
    },
    FACET_ITEMS: {},
    CAPTION:     {
        CHECKEDCOUNTER_VISIBLE: CTRL_CLASS + "__checkedCounter-visible"
    }
};

/**
 *
 * @constructor
 * @extends {HTMLElement}
 */
function epsrchFacet() {

    /**
     * Represent facet id that used on facet configs(without ".utouchable")
     * Initialize at __onend
     * @type {string}
     */
    this.configId = new Wf.AutoRwPropDef("");

    /**
     * Transformed facet data for view. Can't be set directly
     * @type {FacetViewData}
     */
    this.viewData = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return this.$_viewData;
        }
    });

    /**
     * Indicate if facet shout render facetItems in lazy way,
     * e.g. first render only visible facetItems, and then rest of them
     * @type {boolean}
     */
    this.lazyRender = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return !this.facetData.isMore && this.viewData.count > MAX_FACETITEMS_VISIBLE && !!this.$_lazyRender;
        },
        /** @this {epsrchFacet}*/
        set: function (value) {
            this.$_lazyRender = value;
        }
    });

    this.facetItemsLimit = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return this.lazyRender ? MAX_FACETITEMS_VISIBLE : MAX_FACETITEMS;
        }
    });

    this.maxFacetItemsLimit = new Wf.AutoRoPropDef(MAX_FACETITEMS);

    /**
     * Initial data object for this facet
     * @type {FacetContainer}
     */
    this.facetData = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return this.$_facetData;
        },
        /** @this {epsrchFacet}*/
        set: function (data) {
            this.$_facetData = data;
            // `viewData` updates on every facetData update and trigger UI updates through WF Data binding
            if (this.$_viewData) this.$_viewData.$updateAll(convertToViewData(data))
        }
    });

    this.facetId = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return this.facetData.id;
        }
    });

    this.facetName = new Wf.PropDef({
        /** @this {epsrchFacet}*/
        get: function () {
            return this.facetData.name;
        }
    });

    /**
     * Container of facet items
     */
    this.itemsContainer = new Wf.AutoChildPropDef();
    this.items = new Wf.AutoChildPropDef();

    this.checkedCounter = new Wf.AutoChildPropDef();

    /**
     * Event is fired when user checks/unchecks some checkbox within facet.
     */
    this.facetclickEvent = new Wf.DomEventDef("b", FacetClickEventArgs);

    this.FacetClickEventArgs = FacetClickEventArgs;

    function FacetClickEventArgs(options, cause) {
        /**
         * Id of the facet value being clicked.
         */
        this.facetItemId = options.facetItemId;

        this.facetId = options.facetId;

        /**
         * New (current) state of the checkbox over facet value.
         */
        this.checked = options.facetItemChecked;

        /**
         * Cause of this event, typical original DOM 'click' event
         */
        this.cause = cause;
    }

    /**
     * Flag if the facet completely hidden from screen
     */
    this.isHidden = new Wf.PropDef({
        get: function () {
            return !!this.$_isHidden;
        },
        set: function (value) {
            this.$_isHidden = value
        }
    });
    /**
     * Hides control.
     */
    this.hide = function epsrchFacet_hide() {
        if (this.isHidden)
            return;

        this.classList.add(CLASSES.FACET.HIDDEN);
        this.isHidden = true;
    };
    /**
     * Shows previously hidden control.
     */
    this.unhide = function epsrchFacet_unhide() {
        if (!this.isHidden)
            return;

        this.classList.remove(CLASSES.FACET.HIDDEN);
        this.isHidden = false;
    };

    /**
     * If `true` - only facet caption is visible
     */
    this.isFacetCollapsed = new Wf.PropDef({
        get: function () {
            return !!this.$_isFacetCollapsed;
        },
        set: function (state) {
            this.$_isFacetCollapsed = state;
        }
    });

    /**
     * Collapses facets so that only its caption is visible
     */
    this.collapse = function epsrchFacet_collapse() {
        if (this.isFacetCollapsed)
            return;

        this.$wfc.classList.add(CLASSES.FACET.COLLAPSED);
        this.isFacetCollapsed = !this.isFacetCollapsed;
    };

    /**
     * Expands facet to its default state
     */
    this.expand = function epsrchFacet_expand() {
        if (!this.isFacetCollapsed)
            return;

        this.$wfc.classList.remove(CLASSES.FACET.COLLAPSED);
        this.isFacetCollapsed = !this.isFacetCollapsed;
    };

    // TODO: add jsdoc and types
    this.checkedItems = new Wf.PropDef({
        /** @this {epsrchFacet} */
        get: function () {
            var ctrl = this;
            var checkedItems = _.filter(ctrl.viewData.items, {checked: true}).reduce(function (result, item) {
                result[item.name] = {
                    name:    item.name,
                    checked: true
                };
                return result;
            }, {});
            return {
                facets:  checkedItems,
                name:    ctrl.facetName,
                facetId: ctrl.facetId
            };
        }
    });

    this.toggleCollapseFacet = function () {
        /** @type epsrchFacet */
        var ctrl = this.$wfc;
        ctrl.isFacetCollapsed ? ctrl.expand() : ctrl.collapse();
    };

    this.applyFilter = function epsrchFacet_applyFilter(text) {
        var facetItems = this.items,
            regexp = e3suiQuickSearch.getSearchRegexp(text),
            cVisible = 0;
        // TODO: Not reviewed
        facetItems.forEach(function (facetItem) {
            var $li = $(facetItem);
            var $label = $("label", $li);
            var caption = $label.text();

            if (caption.match(regexp)) {
                $li.addClass(CLASSES.FACET_ITEM.FORCE_SHOW)
                   .removeClass(CLASSES.FACET_ITEM.FORCE_HIDE);
                $label.html(e3suiQuickSearch.highlight(caption, regexp, CLASSES.FACET_ITEM.FORCE_FILTER_HIGHLIGHT));
                cVisible++;
            }
            else {
                $li.addClass(CLASSES.FACET_ITEM.FORCE_HIDE)
                   .removeClass(CLASSES.FACET_ITEM.FORCE_SHOW);
            }
        });

        $(this).toggleClass(CLASSES.FACET.FILTERED, cVisible < facetItems.length);

        return cVisible;
    };

    this.unfilter = function epsrchFacet_unfilter() {
        if (!$(this).hasClass(CLASSES.FACET.FILTERED))
            return false;
        // TODO: Not reviewed
        var facetItems = this.items;

        for (var i = 0; i < facetItems.length; i++) {
            var $li = $(facetItems[i]);
            var $label = $("label", $li);
            var caption = $label.text();

            $li.removeClass(CLASSES.FACET_ITEM.FORCE_SHOW)
               .removeClass(CLASSES.FACET_ITEM.FORCE_HIDE);
            $label.text(caption);
        }

        $(this).removeClass(CLASSES.FACET.FILTERED);

        return true;
    };

    this.isItemsCollapsed = new Wf.PropDef({
        /** @this {epsrchFacet} */
        get: function () {
            // Default state of facet items
            if (this.$_isItemsCollapsed == null) {
                this.$_isItemsCollapsed = !this.facetData.isMore;
            }
            return this.$_isItemsCollapsed;
        },
        set: function (newState) {
            return this.$_isItemsCollapsed = newState;
        }
    });

    this.toggleCollapseItems = function (event, showMore, forced) {
        if (event) event.stopPropagation();
        /** @type epsrchFacet */
        var ctrl = this.$wfc;
        if (ctrl.isItemsCollapsed) {
            // Expand all facets items
            ctrl.viewData.$set("itemsExpandText", "Hide");
            ctrl.classList.remove(CLASSES.FACET.ITEMS_COLLAPSED);
        } else {
            // Collapse facets items
            ctrl.viewData.$set("itemsExpandText", "All " + ctrl.viewData.count);
            ctrl.classList.add(CLASSES.FACET.ITEMS_COLLAPSED);
        }
        ctrl.facetData.isMore = ctrl.isItemsCollapsed;
        ctrl.isItemsCollapsed = !ctrl.isItemsCollapsed;

        //TODO: change temp fix. Move to api or event
        var reqView = $page.views["~/epam/search/v-epsrchRequest"];
        if (reqView) reqView.facets[ctrl.facetData.id].isMore = ctrl.facetData.isMore;
    };

    this.__onstart = function () {
        // Initial setup of view data
        this.$_viewData = convertToViewData(this.facetData);
    };

    this.__onend = function () {
        var ctrl = this;
        ctrl.configId = ctrl.facetId.replace(FACET_ID_POSTFIX, "");
        _initFacetBindingHandler(ctrl);
        if (ctrl.lazyRender) {
            facetsApi.deferFacetRender(ctrl);
        }
    };
}

/**
 * Convert facet data to suitable for view format
 * @param {FacetContainer} data
 * @returns {FacetViewData}
 */
function convertToViewData(data) {
    var clonedData = _.clone(data);
    var facetItemsTuple = _.chain(_.values(clonedData.facets))
                           .reduce(function (tuple, item, index) {
                               var styleClass = _resolveFacetItemStyleClass(item, index),
                                   itemId = _resolveFacetItemId(clonedData.id, item.id);
                               tuple.items[itemId] = {
                                   styleId:    itemId,
                                   name:       item.name,
                                   count:      item.count,
                                   order:      item.order,
                                   checked:    item.checked || false,
                                   styleClass: styleClass
                               };
                               if (item.checked) tuple.checkedCount += 1;
                               return tuple;
                           }, {items: {}, checkedCount: 0}).value();

    var count = Object.keys(facetItemsTuple.items).length;
    return {
        items:                 facetItemsTuple.items,
        count:                 count,
        facetTitle:            clonedData.name,
        checkedCount:          facetItemsTuple.checkedCount,
        isFacetCollapsed:      !!clonedData.collapsed,
        itemsExpandText:       !!clonedData.isMore ? "Hide" : "All " + count,
        isItemsExpandable:     count > MAX_FACETITEMS_VISIBLE,
        itemsExpanderCssStyle: count <= MAX_FACETITEMS_VISIBLE ? "display: none" : ""
    }
}

function _resolveFacetItemStyleClass(item, index) {
    var styleClass = "";
    if (!item.checked && index >= MAX_FACETITEMS_VISIBLE) {
        styleClass = CLASSES.FACET_ITEM.HIDDEN;
        if (item.obsolete) styleClass = styleClass + " " + CLASSES.FACET_ITEM.OBSOLETE;
    } else if (item.obsolete || item.count === 0) {
        styleClass = CLASSES.FACET_ITEM.OBSOLETE;
    }
    return CLASSES.FACET_ITEM.DEFAULT + styleClass;
}

function _resolveFacetItemId(facetId, facetItemId) {
    return [CTRL_CLASS, facetId, facetItemId.replace(/\W/g, "_")].join("__");
}

function _initFacetBindingHandler(ctrl) {
    var facetId = ctrl.facetId;

    ctrl.viewData.$on(".", function (bndEvent, srcUIBind) {
        var facetItem = bndEvent.change.src;
        // Trigger only when changes came from UI and items changed with checked bind
        if (srcUIBind && facetItem && Wf.contains(bndEvent.propPath, "items") && bndEvent.change.set === "checked") {
            var facetItemChecked = facetItem.checked,
                facetItemId = facetItem.name,
                facetItemOrder = facetItem.order;

            requestAnimationFrame(function () {
                _updateFacetItemContainerClass(srcUIBind, facetItemChecked, facetItemOrder);
                _updateCheckedCounter(ctrl);
                emitFacetItemChooseEvent(ctrl, {
                    facetId:          facetId,
                    facetItemId:      facetItemId,
                    facetItemChecked: facetItemChecked
                });
            });
            // trigger when data change directly
        } else if (facetItem.styleId && Wf.contains(bndEvent.propPath, "items")) {

        }
    });
}

function _updateCheckedCounter(ctrl) {
    var count = _.filter(ctrl.viewData.items, {"checked": true}).length;
    ctrl.viewData.$set("checkedCount", count);
}

// TODO: remove when class attribute change will be implemented in WF Data
function _updateFacetItemContainerClass(srcUIBind, facetItemChecked, facetItemOrder) {
    if (srcUIBind) {
        var parentLi = $(srcUIBind.node).parents("li")[0];
        if (!facetItemChecked && facetItemOrder > MAX_FACETITEMS_VISIBLE) {
            parentLi.classList.add(CLASSES.FACET_ITEM.HIDDEN);
        } else if (facetItemChecked) {
            parentLi.classList.remove(CLASSES.FACET_ITEM.HIDDEN);
        }
    }
}

/**
 * Emit Event if facet item was chosen by user input
 * @param {epsrchFacet} ctrl
 * @param {{facetId: string, facetItemId: string, facetItemChecked: string}} options
 * @param {Event} [event]
 */
function emitFacetItemChooseEvent(ctrl, options, event) {
    // raise DOM event according to epsrchFacets.event.facetclick specification. Catched in v-epsrchRequest.wfv
    Wf.emitDomEvent(ctrl, ctrl.facetclickEvent, new ctrl.FacetClickEventArgs(options, event));
}

/**
 * TODO: Return to work state. Integrate with binding
 * Cross-browser click handler on checkbox label with button(ctrl, alt, shift) pressed at the same time.
 * Used as a hack for Firefox label and checkbox click support.
 * @param event
 * @private
 */
function _facet_onClick_label(event) {
    // Firefox fix: CTRL+click on label not fire event on checkbox
    if (event.target.tagName === "LABEL" && (event.altKey || event.shiftKey || event.ctrlKey)) {
        //escape and replace possible quotes in selector
        var checkboxId = $(event.target).attr("for").replace(/('|")/g, "\\$1");
        var siblingCheckbox = $("[id='" + checkboxId + "']")[0]; // get associated checkbox for label
        if (siblingCheckbox) {
            var ctrl = siblingCheckbox.$wfc;
            siblingCheckbox.checked = !siblingCheckbox.checked; // set explicit checkbox checked
            event.stopPropagation();
            event.preventDefault(); //prevent checkbox click

            emitFacetItemChooseEvent(ctrl, {
                facetId:          ctrl.facetId,
                //facetItemId:      facetItemId,
                facetItemChecked: siblingCheckbox.checked
            });
        }
    }
}

function _registerTemplateFormatter(template) {
    /**
     * Print number value if it not strict equal zero.
     * @constructor
     */
    function NotZeroFormatter(id, args) {
        Wft.Formatter.call(this, id, args);
    }

    Wf.extend(NotZeroFormatter, Wft.Formatter, {
        format: function (ctx, value) {
            return value === 0 ? "" : value;
        }
    });

    template.addFormatter("notzero", NotZeroFormatter);
}

/**
 *
 * @typedef {{items: Object.<string, FacetItem>, count: number, facetTitle: string, checkedCount: number, expandText: string, isCollapsed: boolean, isExpandable: boolean}} FacetViewData
 */

/**
 *
 * @typedef {{collapsed: boolean, facets: Object.<string, FacetItem>, id: string, isMore: boolean, name: string, order: number, keepData: boolean}} FacetContainer
 */

/**
 *
 * @typedef {{count: number, id: string, name: string, order: number}} FacetItem
 */
