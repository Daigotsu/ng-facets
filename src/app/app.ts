import {FacetListComponent} from "./facets/facet-list.component.ts";

import {Component} from 'angular2/core';
import {FORM_PROVIDERS} from 'angular2/common';
import {FacetListContainer} from "./facets/facetlist";
import {FacetService} from "./facets/facet.service";

require('./e3s/reui/less/styles.less');
require('./e3s/e3suiVariables.less');
require('./e3s/e3suiIconFont.less');

require("./facets/epsrchFacet.less");
require("./facets/epsrchFacets.less");


/*
 * App Component
 * Top Level Component
 */
@Component({
    selector:   'app',
    providers:  [...FORM_PROVIDERS, FacetService],
    directives: [FacetListComponent],
    pipes:      [],
    styles:     [
        `
          header h1 {
            margin-top: 0;
            padding-left: 10px;
          }
          main {
            padding: 15px 10px;
          }
          .test-controls {
            margin-bottom: 1em;
          }
        `
    ],
    template:   require('./app.html')
})

export class App {
    public name = 'Ng-facets';
    facetContainer: FacetListContainer;

    constructor(private _facetService: FacetService) { }

    setFacetData(key) {
        this.facetContainer = this._facetService.getFacetData(key);
    }
}
