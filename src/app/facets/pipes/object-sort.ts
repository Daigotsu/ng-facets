import {PipeTransform} from "angular2/core";
import {Pipe} from "angular2/core";
import * as _ from "lodash"

@Pipe({ name: 'sortBy',  pure: false })
export class SortByPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
        if (!args) return value;
        let sortKey = args[0];
        if (!sortKey) return value;
        return _.sortBy(value, sortKey);
    }
}