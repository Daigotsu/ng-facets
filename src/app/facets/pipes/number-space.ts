import {PipeTransform} from "angular2/core";
import {Pipe} from "angular2/core";

@Pipe({ name: 'numberspace',  pure: true })
export class NumberSpacePipe implements PipeTransform {
    transform(value: any, args: any[] = [3]): any {
        // For best understanding see https://www.debuggex.com/r/2gP26z23BSsDfKZZ
        let digitCount = args[0];
        let regexp = "\\B" +
            "(?=" +
            "(\\d{"+digitCount+"})+" +
            "(?!\\d)" +
            ")";
        return value.toString().replace(new RegExp(regexp, "g"), " ");
    }
}