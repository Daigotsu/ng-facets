// http://stackoverflow.com/a/34074484

import {PipeTransform} from "angular2/core";
import {Pipe} from "angular2/core";

@Pipe({ name: 'values',  pure: false })
export class ValuesPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
        return Object.keys(value).map(key => value[key]);
    }
}