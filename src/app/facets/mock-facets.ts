import {Facet} from "./facet";
import {FacetListContainer} from "./facetlist";

const _mock = require("./allfacetsData.json");

export var FakeFacetList: {[id: string]: FacetListContainer} = {
    "newData": _mock["newData"],
    "oldData": _mock["oldData"],
    "simple":  _mock["simple"]
};
