import {Component, OnInit, NgZone} from "angular2/core";
import {FacetService} from "./facet.service";
import {FacetComponent} from "./facet.component";
import {FacetListContainer} from "./facetlist";
import {Input} from "angular2/core";
import {SortByPipe} from "./pipes/object-sort";
import {ValuesPipe} from "./pipes/object-values";
import {ChangeDetectionStrategy} from "angular2/core";


@Component({
    selector:   "epsrchFacets",
    providers:  [FacetService],
    directives: [FacetComponent],
    pipes:      [SortByPipe, ValuesPipe],
    styles:     [],
    template:   require('./facetList.html'),
    changeDetection: ChangeDetectionStrategy.CheckAlways
})

export class FacetListComponent {
    @Input() public facetsContainer: FacetListContainer;

    facetsKeys() {
        return this.facetsContainer && this.facetsContainer.facets ? Object.keys(this.facetsContainer.facets) : [];
    }

    constructor(private _facetService: FacetService) {
    }

    getFacetListContainer() {
        this.facetsContainer = this._facetService.getFacetData("simple");
    }
}
