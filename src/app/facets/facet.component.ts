import {Component} from "angular2/core";
import {Facet} from "./facet";
import {FacetView} from "./facetView";
import {FacetItem} from "./facetItem";
import {Input} from "angular2/core";
import {FacetViewItem} from "./FacetViewItem";
import {ValuesPipe} from "./pipes/object-values";
import * as _ from "lodash"
import {NumberSpacePipe} from "./pipes/number-space";
import {ChangeDetectionStrategy} from "angular2/core";

// By default only this count of facet items visible on UI
const MAX_FACETITEMS_VISIBLE = 6;
// Max possible count of visible facet items on UI
const MAX_FACETITEMS = 300;

const FACET_ID_POSTFIX = ".untouchable";

const CTRL_CLASS = "epsrchFacet",
    FACET_ITEM = CTRL_CLASS + "__facetItem";

const CLASSES = {
    FACET:       {
        HIDDEN:          CTRL_CLASS + "-hidden",
        COLLAPSED:       CTRL_CLASS + "-collapsed",
        FILTERED:        CTRL_CLASS + "-filtered",
        ITEMS_COLLAPSED: CTRL_CLASS + "-itemsCollapsed"
    },
    FACET_ITEM:  {
        DEFAULT:                "epsrchFacet__items epsrchFacet__facetItem ", // Used for class binding only
        HIDDEN:                 FACET_ITEM + "-hidden",
        OBSOLETE:               FACET_ITEM + "-obsolete",
        FORCE_HIDE:             FACET_ITEM + "-forceHide",
        FORCE_SHOW:             FACET_ITEM + "-forceShow",
        FORCE_FILTER_HIGHLIGHT: FACET_ITEM + "-filterHighlight",
        FORCE_FILTER_OBSOLETE:  FACET_ITEM + "-obsolete"
    },
    FACET_ITEMS: {},
    CAPTION:     {
        CHECKEDCOUNTER_VISIBLE: CTRL_CLASS + "__checkedCounter-visible"
    }
};

@Component({
    selector:  "epsrchFacet",
    providers: [],
    pipes:     [ValuesPipe, NumberSpacePipe],
    styles:    [],
    template:  require('./facet.html')
})
export class FacetComponent {
    private _facetData: Facet;
    @Input() set facetData(data: Facet) {
        this._facetData = data;
        this.viewData = FacetComponent.transform(this._facetData);
        this.isItemsCollapsed = !this._facetData.isMore;
        this.isFacetCollapsed = !!this._facetData.collapsed;
    }

    public viewData: FacetView;
    public isItemsCollapsed: boolean;
    public isFacetCollapsed: boolean;

    constructor() {
    }

    public facetItemsKeys(): Array<string> {
        return Object.keys(this.viewData.items)
    }

    toggleCollapseItems() {
        this.isItemsCollapsed = !this.isItemsCollapsed;
        this.viewData.itemsExpandText = this.isItemsCollapsed ? `All ${this.viewData.count}` : "Hide";
    }

    toggleCollapseFacet() {
        this.isFacetCollapsed = !this.isFacetCollapsed;
    }

    getCheckedCount() {
        this.viewData.checkedCount = _.filter(this.viewData.items, _ => _.checked).length;
        return this.viewData.checkedCount
    }


    private static transform(facetData: Facet): FacetView {
        var facetItemsTuple = _.values(facetData.facets).reduce((tuple: {items:any, checkedCount:number}, item, index) => {
            var styleClass = FacetComponent._resolveFacetItemStyleClass(item, index),
                itemId = FacetComponent._resolveFacetItemId(facetData.id, item.id);
            tuple.items[itemId] = {
                styleId:    itemId,
                name:       item.name,
                count:      item.count,
                order:      item.order,
                checked:    item.checked || false,
                styleClass: styleClass
            };
            if (item.checked) tuple.checkedCount += 1;
            return tuple;
        }, {items: {}, checkedCount: 0});
        var count = Object.keys(facetItemsTuple.items).length;

        return {
            items:                 facetItemsTuple.items,
            count:                 count,
            facetTitle:            facetData.name,
            checkedCount:          facetItemsTuple.checkedCount,
            isFacetCollapsed:      !!facetData.collapsed,
            itemsExpandText:       !!facetData.isMore ? "Hide" : "All " + count,
            isItemsExpandable:     count > MAX_FACETITEMS_VISIBLE,
            itemsExpanderCssStyle: count <= MAX_FACETITEMS_VISIBLE ? "display: none" : ""
        }
    }

    private static _resolveFacetItemStyleClass(item: FacetItem, index: number) {
        var styleClass = "";
        if (!item.checked && index >= MAX_FACETITEMS_VISIBLE) {
            styleClass = CLASSES.FACET_ITEM.HIDDEN;
            if (item.obsolete) styleClass = styleClass + " " + CLASSES.FACET_ITEM.OBSOLETE;
        } else if (item.obsolete || item.count === 0) {
            styleClass = CLASSES.FACET_ITEM.OBSOLETE;
        }
        return styleClass;
    }

    private static _resolveFacetItemId(facetId: string, facetItemId: string) {
        return [CTRL_CLASS, facetId, facetItemId.replace(/\W/g, "_")].join("__");
    }
}