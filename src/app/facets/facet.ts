import {FacetView} from "./facetView";
import {FacetItem} from "./facetItem";

export class Facet {
    collapsed: boolean;
    name: string;
    id: string;
    order: number;
    isMore: boolean = false;
    facets: {[id: string] : FacetItem};
}