export interface FacetViewItem {
    styleId:    string;
    name:       string;
    count:      number;
    order:      number;
    checked:    boolean;
    styleClass: string;
}