import {Facet} from "./facet";

export class FacetListContainer {
    catalog: string;
    facetSortersPaths: Array<string>;
    facets: {[id: string] : Facet}
}