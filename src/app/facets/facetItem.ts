export interface FacetItem {
    count: number;
    name: string;
    id: string;
    order: number;
    checked?: boolean;
    obsolete?: boolean;
}