import {FakeFacetList} from "./mock-facets";
import {Injectable} from "angular2/core";
import {FacetListContainer} from "./facetlist";

@Injectable()
export class FacetService {

    constructor() {
    }

    public getFacetData(key: string = "simple"): FacetListContainer {
        return FakeFacetList[key];
    }
}
