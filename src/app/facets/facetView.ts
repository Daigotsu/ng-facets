import {FacetViewItem} from "./FacetViewItem";

export class FacetView {
    items:  {[id: string] : FacetViewItem};
    count: number;
    facetTitle: string;
    checkedCount: number;
    isFacetCollapsed: boolean;
    itemsExpandText: string;
    isItemsExpandable: boolean;
    itemsExpanderCssStyle: string;
}